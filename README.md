Description: 

Issues:
	
Future Improvements and Considerations:
	-Replace the SearchBar View with appropriate View (such as an Entry View)
	-Add in additional information such as the daily stock change in percentage.
	-Figure out a way to add unique functionality to this app
	
## SimpleCalculator
* A simple Xamarin Forms Mobile App that collects stock data for an inputted stock symbol and presents the data.
* * *
### SnapShot(s)
![IMAGE](https://i.imgur.com/Mel0rfqm.png)
![IMAGE](https://i.imgur.com/GeuawsGm.png)
* * *
### Issues
* Not tested on iOS (need iOS device, simulator, or MacOS computer).
* The UI needs reworking, too much data is cluttered, need more spacing.
* When user clicks on a listview viewcell, the viewcell highlights orange (notice the thin orange line upon tapping a viewcell).
* * *
### Considerations For Improvement
* The "100 day price history" graph should probably have the latest date on right side and previous dates pan out leftward from there (right to left and not left to right).	
* UI rework
* * *
### Author
* **Shane Laskowski**