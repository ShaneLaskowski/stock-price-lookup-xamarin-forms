﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Midterm.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.ObjectModel;

namespace Midterm
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OverviewPage : ContentPage
	{
        public static ObservableCollection<StockDayData> stockRecordings;
        public static bool JSONdataExtracted = false;

        const string endpointErrorMessage = "{\n    \"Error Message\": \"Invalid API call. Please retry or visit the " +
            "documentation (https://www.alphavantage.co/documentation/) for TIME_SERIES_DAILY.\"\n}";

        public OverviewPage ()
		{
			InitializeComponent ();
        }

        public async void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            stockViewList.IsRefreshing = true; //its refreshing.... wow wee
            var userSymbolTyped = StockSymbolSearchBar.Text;
            HistoricalData symbolSearch = new HistoricalData();

            //76CM08E3BV7259ES
            //string TimeSeriesDailyEndpoint = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=76CM08E3BV7259ES";
            string TimeSeriesDailyEndpoint = CreateEndpointString(userSymbolTyped);

            HttpClient client = new HttpClient();
            var response = await client.GetAsync(TimeSeriesDailyEndpoint);

            if (!response.IsSuccessStatusCode) //checks if it doesnt get HTTP 200 ranged repsonse code
                await DisplayAlert("failure", "The endpoint couldn't send us a response", "Okay");
            else
            {
                string jsonContent = await response.Content.ReadAsStringAsync();

                if (jsonContent == endpointErrorMessage) //sent us error json
                    await DisplayAlert("Failure", "Could not retrieve Stock Symbol Information, check ye spelling", "Okay");
                else
                {
                    symbolSearch = JsonConvert.DeserializeObject<HistoricalData>(jsonContent); //now symbolSearch is a C# object with all the attributes from the JSON 
                    JSONdataExtracted = true;

                    stockRecordings = new ObservableCollection<StockDayData>(TransferStockData(symbolSearch));
                    DisplayHighAndLow(stockRecordings);
                    stockViewList.ItemsSource = stockRecordings;

                    

                }
            }
            stockViewList.IsRefreshing = false;
        }

        //note: what if only 1 stock price is recorded in JSON?
        //what if string to double conversion fails... add exception handling
        private void DisplayHighAndLow(ObservableCollection<StockDayData> stockRecordings)
        {   
            double highestStockPrice = double.Parse(stockRecordings[0].High); 
            double lowestStockPrice = double.Parse(stockRecordings[0].Low); 

            //note---the first iteration of the loop always fails
            for(int i = 0; i < stockRecordings.Count; i++)
            {
                if (double.Parse(stockRecordings[i].High) > highestStockPrice)
                    highestStockPrice = double.Parse(stockRecordings[i].High);

                if (double.Parse(stockRecordings[i].Low) < lowestStockPrice)
                    lowestStockPrice = double.Parse(stockRecordings[i].Low);
            }

            HighestStockPriceLabel.Text = "Highest: $" + highestStockPrice;
            LowestStockPriceLabel.Text = "Lowest: $" + lowestStockPrice;
            HighestStockPriceLabel.IsVisible = true;
            LowestStockPriceLabel.IsVisible = true;
        }

        private List<StockDayData> TransferStockData(HistoricalData symbolSearch)
        {
            List<StockDayData> stockDataToReturn = new List<StockDayData>();

            foreach (KeyValuePair<string, TimeSeriesDaily> stockDay in symbolSearch.TimeSeriesDaily)
            {
                StockDayData stockPricesToAdd = new StockDayData
                {
                    Date = stockDay.Key,
                    High = stockDay.Value.The2High,
                    Low = stockDay.Value.The3Low,
                    Close = stockDay.Value.The4Close,
                    Open = stockDay.Value.The1Open,
                    Volume = stockDay.Value.The5Volume
                };

                stockDataToReturn.Add(stockPricesToAdd);

            }

            return stockDataToReturn;
        }

        private string CreateEndpointString(string userSymbolTyped)
        {
            string partialURLEndpoint = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=";
            string URLAPIKey = "&apikey=76CM08E3BV7259ES";

            string completeEnpointURL = partialURLEndpoint + userSymbolTyped.ToUpper() + URLAPIKey;

            return completeEnpointURL;
        }
    }
}