﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entry = Microcharts.Entry;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SkiaSharp;
using Microcharts;
using System.Collections.ObjectModel;
using Midterm.Models;

namespace Midterm
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChartsPage : ContentPage
	{
		public ChartsPage ()
		{
			InitializeComponent ();
        }

        public void CreateStockChart()
        {
            List<Entry> entryList = new List<Entry>();
            var numberOfStockRecordings = OverviewPage.stockRecordings.Count;
            for(int i = 0; i < numberOfStockRecordings; i++)
            {
                if (i % 5 != 0 && i != 0 & i != numberOfStockRecordings - 1)
                    entryList.Add(new Entry(float.Parse(OverviewPage.stockRecordings[i].High)) { Color = SKColor.Parse("#FFFFFF"), });
                else entryList.Add(new Entry(float.Parse(OverviewPage.stockRecordings[i].High))
                        { ValueLabel = OverviewPage.stockRecordings[i].Date, Color = SKColor.Parse("#FFFFFF"), });
            }

            var barChart = new LineChart() {
                Entries = entryList,
                LineMode = LineMode.Straight,
                LineSize = 4,
                PointMode = PointMode.None,
                BackgroundColor = SKColor.Parse("#444444"),
            };

            StockChart1.Chart = barChart;
        }

        private void ChartPage_Appearing(object sender, EventArgs e)
        {
            if (OverviewPage.JSONdataExtracted == true)
            {
                CreateStockChart();
                ChartPageWarningText.IsVisible = false; //probably want to put this statement somewhere else
                chartHeadingLabel.IsVisible = true;
            }
            /*else
                DisplayAlert("Alert!!!", "You must successfully search a stock symbol before viewing charts", "Okay");
                */
        }
    }
}