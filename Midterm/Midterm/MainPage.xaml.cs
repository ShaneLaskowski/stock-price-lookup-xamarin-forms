﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Midterm
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void TabbedPage_Appearing(object sender, EventArgs e)
        {
            //page is loaded up...do something

        }

        private void TabbedPage_Disappearing(object sender, EventArgs e)
        {
            //page is tossed away... do something

        }
    }
}
