﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Midterm.Models
{
    public partial class StockDayData
    {
        public string Date { get; set; }
        public string Open { get; set; }
        public string High { get; set; }
        public string Low { get; set; }
        public string Close { get; set; }
        public long Volume { get; set; }

    }
}
